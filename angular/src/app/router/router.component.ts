import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-router',
  templateUrl: './router.component.html',
  styleUrls: ['./router.component.scss']
})
export class RouterComponent implements OnInit {

  token: any;

  constructor(private router: Router, private auth: AuthService) { }

  ngOnInit(): void {
    this.token = this.auth.jwtTokenDecode()
    if(this.token) {
      switch(this.token.role) {
        case 'user':
          console.log(this.token)
          this.router.navigate(['/apps']);
        break;
        case 'admin': 
          this.router.navigate(['/apps']);
        break;
      }
    } else {
      this.router.navigate(['/login']);
    }
  }

}
