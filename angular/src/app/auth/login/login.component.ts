import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  show: boolean = false;
  jbbData:any = null;
  isAuthenticated:boolean = false;
  internalConnexion:boolean = false;
  welcomeMessage:String = '';
  jwtDecoded: any;

  myClass = 'background-default';

  account_validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required' },
      { type: 'pattern', message: 'Enter a valid email' }
    ],
    'password': [
      { type: 'required', message: 'Password is required' }
    ]
  }

  userData$: Observable<any>;
  OCisAuthenticated:boolean = false;

  // isAuthenticated = false;

  constructor(private auth:AuthService, private router:Router, private fb: FormBuilder, private toastr: ToastrService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.myClass = 'background-default';
    if(this.auth.userIsLoggedIn()) {
      this.refreshFlags();
      this.router.navigate(['/']);
    } else {
      if(!this.OCisAuthenticated || !this.auth.userIsLoggedIn()) {
        this.createForms();
      }
    }
    
  }

  internalConnect(){
    this.internalConnexion = true;
  }

  backConnect(){
    this.internalConnexion = false;
  }

  createForms() {
    // user links form validations
    this.loginForm = this.fb.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required
      ]))
    })
  }

  refreshFlags() {
    this.isAuthenticated = true;
    this.welcomeMessage = 'Welcome';
  }

  login(loginForm: any) {
    this.auth.login(loginForm).subscribe(
      data  => this.handlerLoginSuccess(data),
      error => {
        console.log(error)
        this.handlerError(error.error)
      }
    );
  }

  jwtDecode() {
    this.jwtDecoded = this.auth.jwtTokenDecode();
  }

  handlerError(error: any) {
    // this.snackBar.openSnackBar(error.message,'Close','failed');
    this.toastr.error(error.message, 'Login')
    this.OCisAuthenticated = false;
    this.createForms();
  }

  handlerLoginSuccess(data: any) {
    this.jbbData = data;
    this.refreshFlags();
    sessionStorage.setItem('jbb-data', JSON.stringify(this.jbbData))
    this.jwtDecode();
    if(this.jwtDecoded['role']) {
      switch(this.jwtDecoded['role']) {
        case 'user':
          this.router.navigate(['/apps']);
        break;
        case 'admin': 
          this.router.navigate(['/apps']);
        break;
      }
    }
  }
}
