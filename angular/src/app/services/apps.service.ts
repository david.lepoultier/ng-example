import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { AuthService } from '../auth/auth.service'

const endpoint = {
  "api": environment.apiUrl,
  "auth": environment.authUrl
}

@Injectable({
  providedIn: 'root'
})
export class AppsService {

  constructor(private http: HttpClient, private auth:AuthService) { }

  private getHeaders() {
    let token: any = JSON.parse(this.auth.userIsLoggedIn() || '{}');    
    let httpOptions = {
      headers: new HttpHeaders({
        'x-access-token': token['token'],
        'Content-Type':  'application/json'
      })
    };
    return httpOptions;
  }
  
  private extractData(res: Object) {
    let body = res;
    return body || { };
  }

  getApps(): Observable<any> {
    return this.http.get(endpoint.api + `apps`, this.getHeaders()).pipe(
      map(this.extractData));
  }
}
