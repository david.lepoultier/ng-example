import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppsComponent } from './apps.component';
import { ListAppsComponent } from './list-apps/list-apps.component';

const routes: Routes = [
  { 
    path: 'apps',  
    component: AppsComponent,
    children: [
      {
        path: '',
        children: [
          { path: '', component: ListAppsComponent }
        ]
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
