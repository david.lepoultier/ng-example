import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from 'src/app/services/user.service'
import { AppsService } from 'src/app/services/apps.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ToastrService } from 'ngx-toastr';


export interface AppElement {
  create_at: Date;
  name: string;
  owner: string;
  type: string;
}

@Component({
  selector: 'app-list-apps',
  templateUrl: './list-apps.component.html',
  styleUrls: ['./list-apps.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class ListAppsComponent implements OnInit {

  ELEMENT_DATA: AppElement[];

  pageSizeOptions=[10, 25, 50, 100];
  dataSource: MatTableDataSource<AppElement>;
  columnsToDisplay = ['created_at', 'name', 'owner', 'type'];
  expandedElement: AppElement;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  searchText: string;
  myClass = '';

  constructor(private apps: AppsService, private toastr: ToastrService) { }
  
  ngOnInit() {
    window.scrollTo(0, 0);
    this.myClass = '';
    this.getApps();
  }

  getApps(){
    this.apps.getApps().subscribe(
      data => {
        this.ELEMENT_DATA = data.apps;
        this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
