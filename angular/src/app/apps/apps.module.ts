import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../material-module';
import { MatNativeDateModule } from '@angular/material/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UsersRoutingModule } from './apps-routing.module';
import { SharingModule } from '../sharing-module';

import { AppsComponent } from './apps.component';
import { ListAppsComponent } from './list-apps/list-apps.component';

@NgModule({
  declarations: [ListAppsComponent, AppsComponent],
  imports: [
    CommonModule,
    MaterialModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
    SharingModule,
    UsersRoutingModule
  ],
  entryComponents: [],
  providers: [],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AppsModule { }
