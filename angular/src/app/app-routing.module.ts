
import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageNotFoundComponent }    from './page-not-found/page-not-found.component';
import { LoginComponent } from './auth/login/login.component';
import { StatusComponent } from './status/status.component';
import { RouterComponent } from './router/router.component';

const appRoutes: Routes = [
    {
      path: 'login',
      component: LoginComponent
    },
    {
      path: 'status',
      component: StatusComponent
    },
    {
      path: '',
      component: RouterComponent
    },
    { path: '',
      redirectTo: '/',
      pathMatch: 'full'
    },
    { path: '**', component: PageNotFoundComponent }
  ];
  
@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false, // <-- debugging purposes only
        // preloadingStrategy: SelectivePreloadingStrategyService,
      }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
