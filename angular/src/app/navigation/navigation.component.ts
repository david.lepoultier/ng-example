import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {
  
  public onToggleSidenav = () => {
    
  }
  
  constructor(public auth:AuthService, private router:Router) { }

  appConfig = {
    name: environment.name,
  }

  activeLink = "";
  navigationSideMenu: any = [];
  navigationPages = [];
  navigationOtherSideMenu: any = [];
  jwtDecoded: any;

  navigation: any = [];

  navigationAdmin = [
    { link: 'apps', label: 'Applications' },
    { link: 'users', label: 'Users' },
  ];
  navigationProfile = [
    { link: '/profile', label: 'Profile' }
  ];
  navigationLogin = [
    { link: 'login', label: 'Login', icon: 'user' }
  ];
  navigationDefault = [
    { link: 'apps', label: 'Applications' },
  ];
  navigationUser = [
    { link: 'users', label: 'Users' },
  ];

  userData$: Observable<any>;
  OCisAuthenticated:boolean = false;

  ngOnInit() {
    this.navPageDefault();
  }

  navPageDefault () {
    if(this.auth.userIsLoggedIn()) {
      this.jwtDecode();
      if(this.jwtDecoded['role'] == 'admin') {
        switch(location.pathname){
          case '/users':
            this.navigation = [
              ...this.navigationUser
            ];
            this.activeLink = 'users'
          break;
          default: 
            this.navigation = [
              ...this.navigationDefault
            ];
            this.activeLink = 'apps'
          break;
        }
      } else {
        this.navigation = [
          ...this.navigationDefault
        ];
        this.activeLink = 'apps'
      }
    } else {
      this.navigation = [
        ...this.navigationDefault
      ];
      this.activeLink = 'apps'
    }
  }

  navSelect (page: string) {
    if(this.auth.userIsLoggedIn()) {
      this.jwtDecode();
      if(this.jwtDecoded['role'] == 'admin') {
        switch(page){
          case 'users':
            this.navigation = [
              ...this.navigationUser
            ];
            this.activeLink = 'users'
          break;
          default: 
            this.navigation = [
              ...this.navigationDefault
            ];
            this.activeLink = 'apps'
          break;
        }
      } else {
        this.navigation = [
          ...this.navigationDefault
        ];
        this.activeLink = 'apps'
      }
    }
  }
  
  jwtDecode() {
    this.jwtDecoded = this.auth.jwtTokenDecode();
  }

  logout(){
    this.auth.logout();
  }

  checkAuth() {
    if(this.auth.userIsLoggedIn()) {
      this.jwtDecode();
      if(this.jwtDecoded['role'])
        switch(this.jwtDecoded['role']) {
          case 'user':
            this.navigationSideMenu = [
              ...this.navigationDefault
            ];
            this.navigationOtherSideMenu = [
              ...this.navigationProfile
            ];
          break;
          case 'admin': 
            this.navigationSideMenu = [
              ...this.navigationAdmin
            ];
            this.navigationOtherSideMenu = [
              ...this.navigationProfile
            ];
          break;
          default:
            this.navigationSideMenu = [
              ...this.navigation
            ];
            this.navigationOtherSideMenu = [
              ...this.navigationProfile
            ];
          break;
        }
      return true;
    } else {
      this.navigationSideMenu = [
        // ...this.navigation
      ];
      this.navigationOtherSideMenu = [
        ...this.navigationLogin
      ];
      return true;
    }
  }
}
