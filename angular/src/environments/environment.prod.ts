declare const window: any;

export const environment = {
  production: true,
  apiUrl: window["env"]["backendApiUrl"],
  authUrl: window["env"]["backendAuthUrl"],
  name: window["env"]["name"]
};



