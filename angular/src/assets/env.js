(function(window) {
  window["env"] = window["env"] || {};

  // Environment variables
  window["env"]["backendApiUrl"] = "http://localhost:8080/api/";
  window["env"]["backendAuthUrl"] = "http://localhost:8080/api/oauth/";
  window["env"]["name"] = "EXAMPLE";

})(this);
