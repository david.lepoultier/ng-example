(function(window) {
  window["env"] = window["env"] || {};

  // Environment variables
  window["env"]["backendApiUrl"] = "${BACKEND_API_URL}";
  window["env"]["backendAuthUrl"] = "${BACKEND_AUTH_URL}";
  window["env"]["name"] = "${NAME}";

})(this);
