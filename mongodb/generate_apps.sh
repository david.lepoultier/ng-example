#/bin/sh
i=1
number=1000

while [ $i -lt $number ]
do
  if [ $i -lt 10 ]
  then
    count="00$i"
  fi
  if [ $i -lt 100 ] && [ $i -gt 9 ]
  then
    count="0$i"
  fi
  if [ $i -gt 99 ]
  then
    count=$i
  fi

  d=$((number - i))
  mDate=$(date  -v -${d}H -v -${d}M -v -${d}${i}S '+ISODate("%Y-%m-%dT%H:%M:%S.000Z")')

  echo -n '
    { id: "'$(openssl rand -hex 16)'", 
      name: "applications'$count'", 
      owner: "user'$count'@example.com",
      type: "cloud",
      created_at: '$mDate'
    },'
  i=$((i + 1))
done

