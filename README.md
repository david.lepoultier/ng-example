# NgExample

This project is used to explained how to deploy with **docker-compose**:

* A Frontend Angular
* An API Backend Nodejs
* A MongoDB
* A Treafik Load Balancer to expose all services in HTTPS.

## Demo

Demo environment:

* [Frontend demo](https://ng-example.orangeadd.com)
* [Backend API demo](https://ng-example.orangeadd.com/api/status)

### Credentials

* login: **admin@example.com**
* Password: **Password1**

## Start the development environment

### Prerquis

On your development environment, you must have:

* **Docker** installed ([Install Docker](https://docs.docker.com/get-docker/))
* **Docker Compose** installed ([Install Docker Compose](https://docs.docker.com/compose/install/))

### MongoDB

Start the database MongoDB

```shell
docker-compose --profile mongo up -d
```

### API Backend

Install all npm and start the backend with nodemon

#### Install Node packages

```shell
cd nodejs
npm ci
```

#### Start backend

```shell
cd nodjs
npm run start:dev
```

### Angular Frontend

Install all npm and start the frontend with ng cli

#### Install Angular packages

```shell
cd angular
npm ci
```

#### Start frontend

```shell
cd angular
ng serve
```

### Test the development environment

#### Access to frontend

You can access to frontend by the url: <http://localhost:4200>

Use the credentials to login into application:

* login: **admin@example.com**
* password: **Password1**

![Frontend-1](images/frontend-1.png)

#### Access to API

You can access directly to API with a Postman or curl command.

You must request an access token to connect to API

```curl
curl --location --request POST 'localhost:8080/api/oauth/token' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "admin@example.com",
    "password": "Password1"
}'
```

```json
{
    "success": true,
    "statusCode": 200,
    "auth": true,
    "token": "eyJhbGciOiJFUzM4NCIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsb2NhbGhvc3Q6ODA4MCIsImlkIjoiNWNjYzRlMDJiYzQ4NjgyZWU4ODJlM2ZmIiwidXNlciI6ImFkbWluQGV4YW1wbGUuY29tIiwibGFzdG5hbWUiOiJleGFtcGxlIiwiZmlyc3RuYW1lIjoiYWRtaW4iLCJleHAiOjE2NTE2NjM2ODMsInJvbGUiOiJhZG1pbiIsImlhdCI6MTY1MTU3NzI4M30.gJUWuxugmM4Qt2EmmkJsAKnjo8WgJgf9mYGNzyTsZXLTWUmSyxqKL-ph9evw6mIIca7mpNYeVWUzCY6YfoEfSo_Fy4-dzxAua0BnLUUpmN5whzDWpg5kIXTwVnXF5hfy"
}
```

Copy the `token`.

Retreive all APPS by the API /api/apps

```shell
curl --location --request GET 'localhost:8080/api/apps' \
--header 'x-access-token: eyJhbGciOiJFUzM4NCIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsb2NhbGhvc3Q6ODA4MCIsImlkIjoiNWNjYzRlMDJiYzQ4NjgyZWU4ODJlM2ZmIiwidXNlciI6ImFkbWluQGV4YW1wbGUuY29tIiwibGFzdG5hbWUiOiJleGFtcGxlIiwiZmlyc3RuYW1lIjoiYWRtaW4iLCJleHAiOjE2NTE2NjM2ODMsInJvbGUiOiJhZG1pbiIsImlhdCI6MTY1MTU3NzI4M30.gJUWuxugmM4Qt2EmmkJsAKnjo8WgJgf9mYGNzyTsZXLTWUmSyxqKL-ph9evw6mIIca7mpNYeVWUzCY6YfoEfSo_Fy4-dzxAua0BnLUUpmN5whzDWpg5kIXTwVnXF5hfy'
```

### Stop all

* Kill the nodemon service
* Kill the ng server

## Start the test environment with docker-compose

You can test you development with docker-compose. The images build will be the sames as production environment.

### Build images

```shell
docker-compose --profile local build
```

### Start services

```shell
docker-compose --profile local up
```

### Test the test environment

You can do the same tests as in `Test the development environment`

### Push your images into docker hub

If all is OK, you can push your images in docker hub

#### Create specific tags

```shell
docker tag ng-example_frontend:latest <dockerRepoName>/<imageName1>:<version>
docker tag ng-example_backend:latest <dockerRepoName>/<imageName2>:<version>
```

#### Push your images

```shell
docker push <dockerRepoName>/<imageName1>:<version>
docker push <dockerRepoName>/<imageName2>:<version>
```

## Deploy in GCP

### Prerequis

* You must have an active **Google Cloud Platform** account. You can create a new one and test **for free 90 days**

## Create Virtual Machine

In GCP, you must create new Virtual Machine to host your application. In this VM:

* Docker must be installed
* Docker Compose must be installed
* Git must be installed

### Create instance

Go to GCP [VM Instances](https://console.cloud.google.com/compute/instances), and create new instance. In the creation form, check on the option **Allow HTTPS traffic**

### Update DNS Provider

When the instance is create, you must update your DNS provider with the public IP of the instance, like that:

`ng-example.orangeadd.com. 10797 IN      A       34.77.52.6`

You can use DIG or nslookup to check the resolve name

```shell
dig ng-example.orangeadd.com A
```

```log
; <<>> DiG 9.10.6 <<>> ng-example.orangeadd.com A
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 62297
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ng-example.orangeadd.com.      IN      A

;; ANSWER SECTION:
ng-example.orangeadd.com. 10797 IN      A       34.77.52.6

;; Query time: 17 msec
;; SERVER: 192.168.1.1#53(192.168.1.1)
;; WHEN: Tue May 03 14:16:32 CEST 2022
;; MSG SIZE  rcvd: 69
```

### Update instance

Connect to your instance with ssh (ssh web console / gcloud compute ssh / ...), and install Docker, Docker Compose and git.

#### Clone the git repository

```shell
git clone https://gitlab.com/david.lepoultier/ng-example.git
cd ng-example
```

### Update the docker-compose.yml file

Update the docker-compose.yml file with your personal setting:

```yaml
version: "3.9"

...

services:
    ...

    # INTERNET SETING WITH TRAEFIK TO EXPOSE ALL SERVICE BEHIND A HTTPS ENDPOINT.
    # TO USE ON A SERVER EXPOSED TO INTERNET ON PORT 443 (Ex: INSTANCE LINUX GCP WITH HTTPS FIREWALL OPEN)
    # CERTIFICAT SSL MANAGED BY TRAEFIK
    # PROFIL INTERNET
    # - Update acme.email: @example.com is not authorized
    traefik:
      image: "traefik:v2.6"
      container_name: "traefik"
      command:
        #- "--log.level=DEBUG"
        ...
        - "--certificatesresolvers.myresolver.acme.email=*example@example.com"*
        ...

    # - Update image
    # - Update backend.rule Host with your FQDN Host(`www.my-domain.com`)
    backend-internet:
      container_name: backend-internet
      image: ** <dockerRepoName>/<imageName2>:<version> **
      labels:
      ...
      - "traefik.http.routers.backend.rule=Host(`ng-example.example.com`) && PathPrefix(`/api/`)"
      ...

    # - Update image
    # - Update frontend.rule Host with your FQDN (www.my-domain.com)
    # - Update environment variables:
    #   - BACKEND_API_URL: backend FQDN
    #   - BACKEND_AUTH_URL: backend FQDN
    frontend-internet:
      container_name: frontend-internet
      image: ** <dockerRepoName>/<imageName1>:<version> **
      labels:
      ...
      - "traefik.http.routers.frontend.rule=Host(`ng-example.example.com`)"
      ...
      environment:
        NAME: EXAMPLE
        BACKEND_API_URL: https://ng-example.example.com/api/
        BACKEND_AUTH_URL: https://ng-example.example.com/api/oauth/
      ...

```

### Start the production environment

```shell
docker-compose --profile internet up -d
```

#### Check the logs

You can check the logs of each containers:

```shell
docker logs traefik
docker logs mongo
docker logs backend-internet
docker logs frontend-internet
```

#### Access to your web site

Example:

* <https://ng-example.orangeadd.com>
* <https://ng-example.orangeadd.com/api/status>

#### Check the SSL Certificates

From your web browser you can check the SSL certificate of your site (<https://ng-example.orangeadd.com>)

![Certificate](images/certificate.png)

Or with the site [SSL Labs](https://www.ssllabs.com/ssltest/)

![SSLLabs](images/ssllabs.png)

---
Voila ;)
