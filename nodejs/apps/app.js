'use strict';

const express = require('express');
const router = express.Router();
const appCfg = require('./lib/config')();
const {check, validationResult} = require('express-validator');


// var VerifyToken = require('../auth/verifyToken');

// router.use(bodyParser.urlencoded({ extended: true }));

// CREATES A NEW APP
router.post('/', [
    check('name', 'name cannot be empty').notEmpty(),
    check('type', 'type cannot be empty').notEmpty(),
    check('owner', 'owner cannot be empty').notEmpty(),
  ], function (req, res) {
    let errors = validationResult(req);
    if (!errors.isEmpty()) 
      return res.status(400).json({success: false, statusCode: 400, message: errors});
    
    if (!checkAdmin(req)) {
      res.status(403).json({success: false, statusCode: 403, message: 'You are not authorized'})
    } else {
      appCfg.createApp(req, function(err, statusCode, body) {
        if (err || statusCode != 201) {
          res.status(statusCode).json({success: false, statusCode: statusCode, message: body});
        } else {
          res.status(statusCode).json({success: true, statusCode: statusCode, app: body});
        }
      });
    }
  }
);

// RETURNS ALL THE APPS IN THE DATABASE
router.get('/', function (req, res) {
  appCfg.getApps(function(err, statusCode, body) {
    if (err || statusCode != 200) {
      res.status(statusCode).json({success: false, statusCode: statusCode, message: body});
    } else {
      res.json({success: true, statusCode: statusCode, apps: body});
    }
  });
});


// GETS A SINGLE APP FROM THE DATABASE
router.get('/:id', function (req, res) {
  appCfg.getAppId(req.params.id, function(err, statusCode, body) {
    if (err || statusCode != 200) {
      res.status(statusCode).json({success: false, statusCode: statusCode, message: body});
    } else {
      res.json({success: true, statusCode: statusCode, app: body});
    }
  });
});

// DELETES A APP FROM THE DATABASE
router.delete('/:id', function (req, res) {
  if (!checkAdmin(req)) {
    res.status(403).json({success: false, statusCode: 403, message: 'You are not authorized'})
  } else {
    appCfg.deleteApp(req.params.id, function(err, statusCode, body) {
      if (err || statusCode != 201) {
        res.status(statusCode).json({success: false, statusCode: statusCode, message: body});
      } else {
        res.status(statusCode).json({success: true, statusCode: statusCode, message: body});
      }
    });
  }
});

// UPDATES A SINGLE APP IN THE DATABASE
router.put('/:id', function (req, res) {
  if (!checkAdmin(req)) {
    res.status(403).json({success: false, statusCode: 403, message: 'You are not authorized'})
  } else {
    appCfg.updateAppId(req.params.id, req.body, function(err, statusCode, body) {
      if (err || statusCode != 201) {
        res.status(statusCode).json({success: false, statusCode: statusCode, message: body});
      } else {
        res.json({success: true, statusCode: statusCode, message: body});
      }
    });
  }
});

module.exports = router;

function checkAdmin(req) {
  if (req.userRole == 'admin') {
    return true
  } else {
    return false
  }
}