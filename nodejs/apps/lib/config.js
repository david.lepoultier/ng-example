'use strict';

const mongoApp = require('./mongo');
const generateSafeId = require('generate-safe-id');
const bcrypt = require('bcryptjs');
const errors = require('../../errors/index')();


class appCfg {
  constructor() { }

  getApps(callback) {
    mongoApp.find({}, {_id: 0, __v: 0}, {sort: {name: 'asc'}}, function (error, body) {
      if (error) {
        errors.errors('500', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else if (!body) {
        errors.errors('303', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else {
        callback(null, 200, body);
      }
    });
  }

  getAppId(id, callback) {
    mongoApp.findOne({$or: [{'id': id}, {'name': id}]}, {_id: 0, __v: 0}, function (error, body) {
      if (error) {
        errors.errors('500', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else if (!body) {
        errors.errors('300', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else {
        callback(null, 200, body);
      }
    });
  }

  updateAppId(id, update, callback) {
    mongoApp.findOne({$or: [{'id': id}, {'name': id}]}, function (error, body) {
      if (error) {
        errors.errors('500', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else if (!body) {
        errors.errors('300', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else {
        mongoApp.updateOne({$or: [{'id': id}, {'name': id}]}, update, function(err) {
          if (err) {
            errors.errors('500', function(res){
              callback('error', res.statusCode, res.message)
            });
          } else {
            callback(null, 201, "App updated.")
          }
        });
      }
    });
  }

  createApp(req, callback) {  
    // Make sure this account doesn't already exist
    mongoApp.findOne({ name: req.body.name }, function (err, app) {
      // Make sure user doesn't already exist
      if (app) {
        errors.errors('609', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else {
        //Create and save the user
        let appId = generateSafeId();
        app = new mongoApp({ 
          id: appId,
          name: req.body.name, 
          owner: req.body.owner, 
          type: req.body.type, 
        });
        app.save(function (err, app) {
          if (err) { 
            console.log(err)
            errors.errors('500', function(res){
              callback('error', res.statusCode, res.message)
            });
          } else {
            callback(null, 201, "App added.")
          }
        });
      }
    });
  }

  deleteApp(id, callback) {
    mongoApp.findOneAndDelete({$or: [{'id': id}, {'name': id}]}, function(err) {
      if (err) {
        errors.errors('304', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else {
        callback(null, 201, "App has been deleted.")
      }
    })
  }
}

module.exports = function() {
  return new appCfg();
};