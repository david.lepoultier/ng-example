
var mongoose = require('mongoose');  
var AppSchema = new mongoose.Schema({
  id: String,
  name: String,
  owner: String,
  type: String,
  created_at: { 
    type: Date, 
    default: Date.now 
  }
});
mongoose.model('Applications', AppSchema);

module.exports = mongoose.model('Applications');