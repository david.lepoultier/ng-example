'use strict';

const express = require('express');
const router = express.Router();
const fs = require('fs');
const bodyParser = require('body-parser');
const userCfg = require('../user/lib/config')();

var VerifyToken = require('./verifyToken');

router.use(bodyParser.urlencoded({ extended: false }));

/**
 * Configure JWT
 */
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var bcrypt = require('bcryptjs');
var config = require('../config'); // get config file
var tokenExpir = config.token.tokenExpir;
var privateKey = fs.readFileSync('./auth/jwtES384.key');

router.post('/resetpassword/:id', function(req, res) {
  if (!req.body.password) return res.status(400).send({success: false, statusCode: 400, message: 'Missing new password...'});
  if (!req.body.confirmPassword) return res.status(400).send({success: false, statusCode: 400, message: 'Missing new confirmPassword...'});
  if (!req.body.currentPassword) return res.status(400).send({success: false, statusCode: 400, message: 'Missing old password...'});
  userCfg.getUserById(req.params.id, function(err, statusCode, user) {
    if (err || statusCode != 200) {
      res.status(statusCode).json({success: false, statusCode: statusCode, message: user.message});
    } else {
      // check if the password is valid
      var passwordIsValid = bcrypt.compareSync(req.body.currentPassword, user.password);
      if (!passwordIsValid) return res.status(401).send({success: false, statusCode: 401, message: 'Invalid old password...'});
      if (req.body.password != req.body.confirmPassword) return res.status(400).send({success: false, statusCode: 400, message: 'confirmPassword must be equal to password...'});
      user.password = bcrypt.hashSync(req.body.password, 8);
      userCfg.updateUserId(req.params.id, user, function(err, statusCode, body) {
        if (err || statusCode != 201) {
          res.status(statusCode).json({success: false, statusCode: statusCode, message: body});
        } else {
          res.json({success: true, statusCode: statusCode, user: body});
        }
      });
    }
  });
});

router.post('/login', function(req, res) {
  getToken(req, res);
});

router.post('/token', function(req, res) {
  getToken(req, res);
});

router.get('/logout', function(req, res) {
  res.status(200).send({ auth: false, token: null });
});

router.get('/me', VerifyToken, function(req, res) {
  userCfg.getUserId(req.userId, function (err, status, user) {
    console.log(user)
    if (err) return res.status(500).send("There was a problem finding the user.");
    if (!user) return res.status(404).send("No user found.");
    res.send(user);
  });
});

module.exports = router;

function getToken(req, res) {
  if (!req.body.email) return res.status(400).send({success: false, statusCode: 400, message: 'Missing email...'});
  if (!req.body.password) return res.status(401).send({success: false, statusCode: 401, message: 'Missing password...'});
  userCfg.getUserEmail(req.body.email, function(err, statusCode, user) {
    if (err || statusCode != 200) {
      res.status(statusCode).json({success: false, statusCode: statusCode, message: user.message});
    } else {
      // check if the password is valid
      var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
      if (!passwordIsValid) return res.status(401).send({success: false, statusCode: 401, message: 'Invalid password...'});

      // if user is found and password is valid
      // create a token
      var token = jwt.sign({ 
        iss: req.headers['host'],
        id: user.id,
        user: user.email, 
        lastname: user.lastname,
        firstname: user.firstname,
        exp: Math.floor(Date.now() / 1000) + tokenExpir,
        role: user.role
      }, privateKey, { algorithm: 'ES384'});

      // return the information including token as JSON
      res.status(200).send({success: true, statusCode: 200, auth: true, token: token});
    }
  });
};