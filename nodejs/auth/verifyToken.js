var jwt = require('jsonwebtoken');
var fs = require('fs');
var userCfg = require('../user/lib/config')();
var publicKey = fs.readFileSync('./auth/jwtES384.key.pub');

function verifyToken(req, res, next) {

  // check header or url parameters or post parameters for token
  var token = req.headers['x-access-token'];
  if (!token) 
    return res.status(403).send({ auth: false, message: 'No token provided.' });

    // verifies secret and checks exp
  jwt.verify(token, publicKey, { algorithms: ['ES384'] }, function(err, decoded) {  
    if (err) {
      switch(err.message) {
        case 'jwt expired':
          return res.status(401).send({ auth: false, message: 'Your token has expired.' });
        default:
          return res.status(500).send({ auth: false, message: err });
      }
    }
    req.userId = decoded.user;
    req.userRole = decoded.role;
    next();
  });

}

module.exports = verifyToken;