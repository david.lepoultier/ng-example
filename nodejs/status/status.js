const express = require('express');
const router = express.Router();
const version = require('../package.json');
const mongoUser = require('../user/lib/mongo');


const getStatus = function(callback) {
  let myBody = {
    "name": version.name,
    "status": "ok",
    "version": version.version,
    "db": {
      "database":"mongo",
      "connection": "ko",
      "error": undefined
    }
  };
  mongoUser.findOne({id: '1234'}, function (error, body) {
    if (error) {
      myBody.status = "ko";
      myBody.db.error = error;
      callback(myBody, 500, null);
    } else {
      myBody.db.connection = 'ok'
      callback(null, 200, myBody);
    }
  });
}

router.get('/', (req,res) => {
  getStatus(function(err, statusCode, body) {
    if (err || statusCode != 200) {
      res.status(statusCode).json({success: false, statusCode: statusCode, message: err});
    } else {
      res.json({success: true, status: body});
    }
  });
});

module.exports = router;