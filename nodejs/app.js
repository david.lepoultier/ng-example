const express = require('express');
const app = express();
const db = require('./db');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const Status = require('./status/status');
const Auth = require('./auth/auth');
const User = require('./user/user');
const Applications = require('./apps/app');
const VerifyToken = require('./auth/verifyToken');
const compression = require('compression');
const helmet = require('helmet');
const date = require('date-and-time')
const cors = require('cors');

app.use(compression());
app.use(helmet());
app.use(bodyParser.json());
app.use(cookieParser());

app.use((req, res, next) => {
  let myDate = date.format(new Date(), "YYYY-MM-DD HH:mm:ss");
  console.log(myDate, '--', req.method, req.originalUrl);
  next();
});

app.use(cors())

app.use((req, res, next) => {
  res.header('Content-Type', 'application/json');
  next();
});

app.use('/api/oauth', Auth);
app.use('/api/status', Status);

app.use((req, res, next) => {
  VerifyToken(req, res, next);
});

app.use('/api/users', User);
app.use('/api/apps', Applications);

module.exports = app;