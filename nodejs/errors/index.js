

class errors {
  constructor() { }
  errors(code, callback) {
    let errors = {
      500: {
        statusCode: 500,
        message: {
          message: 'Internal error'
        }
      },
      609: {
        statusCode: 409,
        message: {
          message: 'Application already exist.'
        }
      },
      709: {
        statusCode: 409,
        message: {
          message: 'Email address already associated to another account.'
        }
      },
    }
    let getError = errors[code]
    callback(getError);
  }
  
}

module.exports = function() {
  return new errors();
};