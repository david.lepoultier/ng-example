'use strict';

const express = require('express');
const router = express.Router();
const userCfg = require('./lib/config')();
const {check, validationResult} = require('express-validator');


// var VerifyToken = require('../auth/verifyToken');

// router.use(bodyParser.urlencoded({ extended: true }));

// CREATES A NEW USER
router.post('/', [
    check('firstname', 'Firstname cannot be empty').notEmpty(),
    check('lastname', 'Lastname cannot be empty').notEmpty(),
    check('email', 'Your email is not valid').isEmail(),
    check('password', 'password cannot be empty').notEmpty()
      .isLength({ min: 4}).withMessage('password must be at least 4 characters'),
    check('confirmPassword', 'confirmPassword cannot be empty').notEmpty()
  ], function (req, res) {
    let errors = validationResult(req);
    if (!errors.isEmpty()) 
      return res.status(400).json({success: false, statusCode: 400, message: errors});
    
    if (req.body.password != req.body.confirmPassword) {
      let confirmPass = {
          errors: {
            msg: 'confirmPassword must be equal to password',
            param: 'confirmPassword',
            location: 'body'
          }
      }
      return res.status(400).json({success: false, statusCode: 400, message: confirmPass});
    }

    if (!checkAdmin(req) && req.body.role == 'admin')
      return res.status(403).json({success: false, statusCode: 403, message: 'You are not admin! You cannot create user with "admin" role'})

    userCfg.createUser(req, function(err, statusCode, body) {
      if (err || statusCode != 201) {
        res.status(statusCode).json({success: false, statusCode: statusCode, message: body});
      } else {
        res.status(statusCode).json({success: true, statusCode: statusCode, user: body});
      }
    });
  }
);

// RETURNS ALL THE USERS IN THE DATABASE
router.get('/', function (req, res) {
  if (!checkAdmin(req)) {
    res.status(403).json({success: false, statusCode: 403, message: 'You are not authorized'})
  } else {
    userCfg.getUsers(function(err, statusCode, body) {
      if (err || statusCode != 200) {
        res.status(statusCode).json({success: false, statusCode: statusCode, message: body});
      } else {
        res.json({success: true, statusCode: statusCode, users: body});
      }
    });
  }
});

// RETURNS ALL THE EMAIL IN THE DATABASE
router.get('/emails', function (req, res) {
  if (!checkAdmin(req)) {
    res.status(403).json({success: false, statusCode: 403, message: 'You are not authorized'})
  } else {
    userCfg.getUsersEmail(function(err, statusCode, body) {
      if (err || statusCode != 200) {
        res.status(statusCode).json({success: false, statusCode: statusCode, message: body});
      } else {
        res.json({success: true, statusCode: statusCode, users: body});
      }
    });
  }
});

// GETS A SINGLE USER FROM THE DATABASE
router.get('/:id', function (req, res) {
  if (!checkAdmin(req)) {
    res.status(403).json({success: false, statusCode: 403, message: 'You are not authorized'})
  } else {
    userCfg.getUserId(req.params.id, function(err, statusCode, body) {
      if (err || statusCode != 200) {
        res.status(statusCode).json({success: false, statusCode: statusCode, message: body});
      } else {
        res.json({success: true, statusCode: statusCode, user: body});
      }
    });
  }
});

// DELETES A USER FROM THE DATABASE
router.delete('/:id', function (req, res) {
  if (!checkAdmin(req)) {
    res.status(403).json({success: false, statusCode: 403, message: 'You are not authorized'})
  } else {
    userCfg.deleteUser(req.params.id, function(err, statusCode, body) {
      if (err || statusCode != 201) {
        res.status(statusCode).json({success: false, statusCode: statusCode, message: body});
      } else {
        res.status(statusCode).json({success: true, statusCode: statusCode, message: body});
      }
    });
  }
});

// UPDATES A SINGLE USER IN THE DATABASE
router.put('/:id', function (req, res) {
  delete req.body.password;
  delete req.body.confirmPassword;
  delete req.body.email;
  if (!checkAdmin(req)) {
    delete req.body.role;
  }
  userCfg.updateUserId(req.params.id, req.body, function(err, statusCode, body) {
    if (err || statusCode != 201) {
      res.status(statusCode).json({success: false, statusCode: statusCode, message: body});
    } else {
      res.json({success: true, statusCode: statusCode, message: body});
    }
  });
});

module.exports = router;

function checkAdmin(req) {
  if (req.userRole == 'admin') {
    return true
  } else {
    return false
  }
}