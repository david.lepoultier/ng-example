var mongoose = require('mongoose');  
var UserSchema = new mongoose.Schema({
  id: String,
  firstname: String,
  lastname: String,
  password: String,
  signature: String,
  role: {
    type: String,
    default: 'user'
  },
  email: { 
    type: String,
    unique: true
  },
  verified: { 
    type: Boolean,
    default: true
  },
  created_at: { 
    type: Date, 
    default: Date.now 
  }
});
mongoose.model('User', UserSchema);

module.exports = mongoose.model('User');