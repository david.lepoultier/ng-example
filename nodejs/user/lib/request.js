'use strict';

const bodyParser = require('body-parser');

const userCfg = require('./config')();
const roleCfg = require('../../roles/lib/config')();
const kong = require('../../kong/lib/kong')();
const orgCfg = require('../../organizations/lib/config')();


class userRequest {
  removeUserByTenantId(tenantId, emails, level, callback) {
    emails.forEach(mail => {
      userCfg.getUserByEmail(mail, function(err, statusCode, result) {
        if(result.tenants) {
          result.tenants.forEach(function(tenant, index) {
            requestRole(tenant.role, function(requestRole) {
              if (level <= requestRole.level) {
                if (tenant.id == tenantId) {
                  result.tenants.splice(index,1);
                  result.save()
                }
              }
            });
          });
        }
      });
    });
    callback(null, 201, 'Member(s) removed')
  }

  addUserByTenantId(tenantId, request, callback) {
    request.body.emails.forEach(mail => {
      userCfg.getUserByEmail(mail, function(err, statusCode, result) {
        if(result.tenants) {
          let addEntry = true;
          result.tenants.forEach(function(tenant) {
            if (tenant.id == tenantId) {
              addEntry = false;
            }
          });
          if (addEntry) {
            let tenant = {
              id: tenantId,
              role: request.body.role
            }
            result.tenants.push(tenant)
            result.save()
            if (request.body.envType && request.body.envType == 'enterprise') {
              let workspace = '';
              if (request.body.workspace) {
                workspace = request.body.workspace;
              } else {
                workspace = null;
              }
              addUserKong(tenantId, workspace, request.body.role, mail, result.apiKey, request);
            }
          }
        }
      });
    });
    callback(null, 201, 'Member(s) added')
  }

  updateUserByTenantId(request, callback) {
    let email = request.params.email;
    let tenantId = request.params.id;
    let role = request.body.role;
    userCfg.getUserByEmail(email, function(err, statusCode, result) {
      let oldRole = '';
      result.tenants.forEach(function(tenant, index) {
        if (tenant.id == tenantId) {
          oldRole = tenant.role;
          result.tenants.splice(index,1);
          return false
        }
      });
      let tenant = {
        id: tenantId,
        role: role,
      }
      result.tenants.push(tenant)
      result.save()
      if (request.body.envType && request.body.envType == 'enterprise') {
        let workspace = '';
        if (request.body.workspace) {
          workspace = request.body.workspace;
        } else {
          workspace = null;
        }
        updateUserKong(tenantId, workspace, role, email, oldRole, request);
      }
    });
    callback(null, 201, 'Member updated')
  }
}

module.exports = function() {
  return new userRequest();
};

function requestRole(role, cb){
  roleCfg.getRole(role, function(err, statusCode, body) {
    cb(body);
  });
};

function addUserKong(tenantId, workspace, roleAdmin, user, apiKey, request) {
  orgCfg.getOrgId(tenantId, function(err, statusCode, body) {
    if (err || statusCode != 200) {
      return err;
    } else {
      if (!body.environment.adminUrl) {
        return 'Kong admin unset';
      } else { 
        let cred = body.adminUser.ADMIN_TOKEN;
        let adminUrl = body.environment.adminUrl 
        let reqUser = { username: user, email: user }
        let reqRbac = { name: user, user_token: apiKey }
        let role = null;
        switch(roleAdmin) {
          case 'admin':
            role = 'admin';
          break;
          case 'owner':
            role = 'super-admin';
          break;
          default:
            role = 'read-only'
          break;
        }
        let reqRole = { roles: role }
        kong.addInternal(adminUrl, 'admins', cred, reqUser, null, request.proxy, function(err, statusCode, body) {
          kong.addInternal(adminUrl, `admins/${user}/roles`, cred, reqRole,  null, request.proxy, function(err, statusCode, body) {});
        });
        kong.addInternal(adminUrl, `rbac/users`, cred, reqRbac, null, request.proxy, function(err, statusCode, body) {
          kong.addInternal(adminUrl, `rbac/users/${user}/roles`, cred, reqRole, null, request.proxy, function(err, statusCode, body) {});
        });
      }
    }
  });
}

function updateUserKong(tenantId, workspace, roleAdmin, user, oldRole, request) {
  orgCfg.getOrgId(tenantId, function(err, statusCode, body) {
    if (err || statusCode != 200) {
      console.log(err)
      return err;
    } else {
      if (!body.environment.adminUrl) {
        console.log('Kong admin unset')
        return 'Kong admin unset';
      } else { 
        let cred = body.adminUser.ADMIN_TOKEN;
        let adminUrl = body.environment.adminUrl 
        let role = null;
        let admin = true;
        let oldAdmin = true;
        switch(roleAdmin) {
          case 'admin':
            role = 'admin';
          break;
          case 'owner':
            role = 'super-admin';
          break;
          default:
            role = 'read-only'
            admin = false;
          break;
        }
        let reqRole = { roles: role }
        switch(oldRole) {
          case 'admin':
            role = 'admin';
          break;
          case 'owner':
            role = 'super-admin';
          break;
          default:
            role = 'read-only'
            oldAdmin = false;
          break;
        }
        let reqOldRole = { roles: role }
        if (oldAdmin)
          kong.removeInternal(adminUrl, `admins/${user}/roles`, cred, reqOldRole,  null, request.proxy, function(err, statusCode, body) {});
        kong.removeInternal(adminUrl, `rbac/users/${user}/roles`, cred, reqOldRole, null, request.proxy, function(err, statusCode, body) {});
        if (admin)
          kong.addInternal(adminUrl, `admins/${user}/roles`, cred, reqRole,  null, request.proxy, function(err, statusCode, body) {});
        kong.addInternal(adminUrl, `rbac/users/${user}/roles`, cred, reqRole, null, request.proxy, function(err, statusCode, body) {});
      }
    }
  });
}