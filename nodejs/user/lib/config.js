'use strict';

const mongoUser = require('./mongo');
const generateSafeId = require('generate-safe-id');
const bcrypt = require('bcryptjs');
const errors = require('../../errors/index')();


class userCfg {
  constructor() { }

  getUsers(callback) {
    mongoUser.find({}, {password: 0, _id: 0, apiKey: 0, signature: 0, __v: 0}, {sort: {email: 'asc'}}, function (error, body) {
      if (error) {
        errors.errors('500', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else if (!body) {
        errors.errors('303', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else {
        callback(null, 200, body);
      }
    });
  }

  getUsersEmail(callback) {
    mongoUser.find({}, {email: 1, _id: 0}, {sort: {email: 'asc'}}, function (error, body) {
      if (error) {
        errors.errors('500', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else if (!body) {
        errors.errors('303', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else {
        let mails = [];
        for (let index = 0; index < body.length; index++) {
          mails.push(body[index]["email"])
        }
        callback(null, 200, mails);
      }
    });
  }

  getUserId(id, callback) {
    mongoUser.findOne({$or: [{'id': id}, {'email': id}]}, {password: 0, _id: 0, __v: 0}, function (error, body) {
      if (error) {
        errors.errors('500', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else if (!body) {
        errors.errors('300', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else {
        callback(null, 200, body);
      }
    });
  }

  updateUserId(id, update, callback) {
    mongoUser.findOne({$or: [{'id': id}, {'email': id}]}, function (error, body) {
      if (error) {
        errors.errors('500', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else if (!body) {
        errors.errors('300', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else {
        mongoUser.updateOne({$or: [{'id': id}, {'email': id}]}, update, function(err) {
          if (err) {
            errors.errors('500', function(res){
              callback('error', res.statusCode, res.message)
            });
          } else {
            callback(null, 201, "User updated.")
          }
        });
      }
    });
  }

  createUser(req, callback) {  
    // Make sure this account doesn't already exist
    mongoUser.findOne({ email: req.body.email }, function (err, user) {
      // Make sure user doesn't already exist
      if (user) {
        errors.errors('709', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else {
        //Create and save the user
        let userId = generateSafeId();
        let apiKey = generateSafeId();
        let hashedPassword = bcrypt.hashSync(req.body.password, 8);
        let signature = generateSafeId()
        user = new mongoUser({ 
          id: userId,
          firstname: req.body.firstname, 
          lastname: req.body.lastname, 
          email: req.body.email, 
          password: hashedPassword,
          signature: signature,
          apiKey: apiKey,
          role: req.body.role
        });
        user.save(function (err, user) {
          if (err) { 
            console.log(err)
            errors.errors('500', function(res){
              callback('error', res.statusCode, res.message)
            });
          } else {
            callback(null, 201, "User added.")
          }
        });
      }
    });
  }

  deleteUser(id, callback) {
    mongoUser.findOneAndDelete({$or: [{'id': id}, {'email': id}]}, function(err) {
      if (err) {
        errors.errors('304', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else {
        callback(null, 201, "User has been deleted.")
      }
    })
  }

  getUserEmail(email, callback) {
    mongoUser.findOne({email: email}, {_id: 0, apiKey: 0, signature: 0, __v: 0}, function (error, body) {
      if (error) {
        errors.errors('500', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else if (!body) {
        errors.errors('301', function(res){
          callback('error', res.statusCode, res.message)
        });
      } else {
        callback(null, 200, body);
      }
    });
  }
}

module.exports = function() {
  return new userCfg();
};