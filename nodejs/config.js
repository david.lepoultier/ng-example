module.exports = {
  token: {
    'tokenExpir': 86400
  },
  mongo: {
    user: process.env.MONGODB_USER,
    password: process.env.MONGODB_PASSWORD,
    host: process.env.DATABASE_SERVICE_NAME,
    port: process.env.MONGODB_PORT,
    database: process.env.MONGODB_DATABASE
  }
};